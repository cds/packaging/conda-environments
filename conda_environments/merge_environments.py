"""
Merge an overlay yaml file onto a conda environment definition file to make a new environment
definition.
"""
import yaml
import os
import os.path as path
import requests
import sys
import click

def parse_dep_list(env: list):
    """
    Re-structure the dependency list of a conda yaml file so that
    the packages are in a dict {<name>: {version: <version>, build: <build>}}
    :param env: A list of dependency definition strings
    :return:
    """
    deps = {}
    for dep_spec in env:
        dep_parts = dep_spec.split('=')
        new_dep = {}
        if len(dep_parts) > 1:
            new_dep["version"] = dep_parts[1]
        if len(dep_parts) > 2:
            new_dep["build"] = dep_parts[2]
        deps[dep_parts[0]] = new_dep
    return deps


def compile_dep_list(deps: dict):
    """
    Recompile a dependency structure as returned from parse_dep_list() into
    a dependency list suitable for use in a yaml file:

    dependencies:
    - cmake:1.2.3:py37hf78fe_1

    or similar

    packages will be in alphabetical order

    :param deps:
    :return:
    """
    keys = sorted(deps.keys())
    dep_list = []
    for key in keys:
        dep_parts = [key]
        dep = deps[key]
        if "version" in dep:
            dep_parts.append(dep["version"])
            if "build" in dep:
                dep_parts.append(dep["build"])
        dep_list.append("=".join(dep_parts))
    return dep_list


def get_yaml_file(file_path: str):
    """
    Return a yaml file contents, first checking if a local file exists at path, then
    trying to get the file as if path were a url
    :param path: a local path or a url
    :return: contents of the file
    """
    if path.exists(file_path):
        with open(file_path, "rt") as f:
            return yaml.safe_load(f)
    else:
        response = requests.request("get",file_path)
        if response.status_code != 200:
            raise Exception(f"Failed to download base environment at {file_path}")
        return yaml.safe_load(response.text)


def merge_environment(export_dir: str, overlays_dir, env: dict):
    """
    Merge the overlay onto the base environment specified in env, creating
    a new environment definition file at <export_dir>/name

    :param export_dir: new file will be <export_dir>/<name>.yaml
    :param env: A dictionary with 'name base_url overlay' as keys defining a merged environment
    :return: None
    """
    sys.stdout.write(f"creating {env['name']} ... ")
    sys.stdout.flush()
    base = get_yaml_file(env["base_url"])

    overlay_path = path.join(overlays_dir, env["overlay"])
    overlay = get_yaml_file(overlay_path)

    base_deps = parse_dep_list(base["dependencies"])
    overlay_deps = parse_dep_list(overlay["dependencies"])

    for name in overlay_deps:
        base_deps[name] = overlay_deps[name]

    # rewrite dependencies for the base
    base["dependencies"] = compile_dep_list(base_deps)

    # also rewrite the name
    base["name"] = env["name"]

    # write out file
    new_file = path.join(export_dir, f"{env['name']}.yaml")
    with open(new_file, "wt") as f:
        yaml.dump(base, f)
    print(f"wrote to {new_file}")


def merge_environments(env_yaml_file: str, overlays_dir: str, export_base_dir: str):
    with open(env_yaml_file, "rt") as f:
        environments = yaml.safe_load(f)

    # clear environments.txt
    env_list = path.join(export_base_dir, "environments.txt")
    with open(env_list, "wt"):
        pass

    for architecture in environments:
        env_path = path.join(export_base_dir, architecture)
        os.makedirs(env_path, exist_ok=True)
        for env in environments[architecture]:
            merge_environment(env_path, overlays_dir, env)

            # write out name to environments.txt
            with open(env_list, "at") as f:
                fname = path.join(architecture, env['name'])
                f.write(f"{fname}\n")

@click.command()
@click.argument("environment_file")
@click.argument("overlay_dir")
@click.argument("export_dir")
def merge_environments_cli(environment_file, overlay_dir, export_dir):
    """
    Create conda environment definitions describe in environment_file by merging overlays found in overlay_dir.
    New definitions are written to export_dir
    """
    merge_environments(environment_file, overlay_dir, export_dir)
from setuptools import find_packages, setup

from setuptools import find_packages, setup

try:
    with open("README.md") as f:
        long_description = f.read()
except Exception:
    long_description = ""
    print("Failed to load README.md as long_description")

setup(
    name="conda_environments",
    author="Erik von Reis",
    packages=find_packages(),
    author_email="evonreis@caltech.edu",
    description="Merge conda environment definition files with overlay files to create new environments",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ligo.org/cds-packaging/conda-environments",
    platforms=["Linux"],
    license="GPL-3",
    install_requires=["requests", "pyyaml", "click"],
    entry_points={
        "console_scripts": [
            "merge-conda-environments = conda_environments.__main__:merge_environments",
        ]
    },
)
# conda-environments

Scripts and data for creating custom conda environments based of certain IGWN environments, but with some customized packages.

## Concept

CDS conda environment definitions are created from a base IGWN environment definitions
merged with a CDS overlay files.

## Usage

```
merge-conda-environments path/to/environments.yaml path/to/overlays path/to/exports
```

## Environment definitions

The cds environments to be created are defined in the environments.yaml file. 
An example file is shown here:

```
linux-64:
  -  name: cds-py37
     base_url: "https://computing.docs.ligo.org/conda/environments/linux/igwn-py37.yaml"
     overlay: "linux-64/py37.yaml"
  -  name: cds-py38
     base_url: "https://computing.docs.ligo.org/conda/environments/linux/igwn-py38.yaml"
     overlay: "linux-64/py38.yaml"
osx-64:
  -  name: cds-py37
     base_url: "https://computing.docs.ligo.org/conda/environments/osx/igwn-py37.yaml"
     overlay: "osx-64/py37.yaml"
  -  name: cds-py38
     base_url: "https://computing.docs.ligo.org/conda/environments/osx/igwn-py38.yaml"
     overlay: "osx-64/py38.yaml"
```

## Overlay files

The overlay files are yaml files in the same format as 
environment definitions.  They are in the ```overlay/<architecture>``` folders.

Entries in the ```dependencies``` section of an overlay file will replace any entries in the 
base IGWN environment file.

### Managing the overlays

To change a package in a CDS environment, find the line in the corresponding overlay file, 
e.g for the cmake package.

```
- cmake=3.21.3=hdbd2f3e_0
```

The text before the first '=' is the package name ```cmake``` 
followed by the package version ```3.21.3```, then finally the build number ```hdbd2f3e_0```

Change the version and/or build number to the new values.  Build number is optional.  
If build number is not included, then version is also optional.  If version or build number are missing,
then conda will install the latest version of the package that is compatible with other packages in the environment.

If the package is not yet in the overlay file, a new line must be added.

When finished, commit and push the changes back to git lab.  Installation of the environment will be 
automatically tested, and the change will propagate to the workstations in a matter of a couple of days.
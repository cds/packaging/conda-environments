import pytest
from conda_environments.merge_environments import merge_environments
import yaml


def test_merge():
    merge_environments("test/environments.yaml", "test/overlays", "test/export")
    with open("test/export/linux-64/cds-py37.yaml") as f:
        new_struct = yaml.safe_load(f)
    with open("test/expected/linux-64/cds-py37.yaml") as f:
        expected_struct =yaml.safe_load(f)

    new_deps = new_struct['dependencies']
    exp_deps = expected_struct['dependencies']

    assert len(new_deps) == len(exp_deps)

    for i in range(len(new_deps)):
        assert new_deps[i].strip() == exp_deps[i].strip()

    with open("test/export/linux-64/cds-py38.yaml") as f:
        new_struct = yaml.safe_load(f)
    with open("test/expected/linux-64/cds-py38.yaml") as f:
        expected_struct =yaml.safe_load(f)

    new_deps = new_struct['dependencies']
    exp_deps = expected_struct['dependencies']

    assert len(new_deps) == len(exp_deps)

    for i in range(len(new_deps)):
        assert new_deps[i].strip() == exp_deps[i].strip()

    with open("test/export/environments.txt") as f:
        envs = [s.strip() for s in f.readlines()]

    assert "linux-64/cds-py38" in envs
    assert "linux-64/cds-py37" in envs
